<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


use Carbon\Carbon;
use Carbon\CarbonInterval;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface
{
    /**
     * @param string $bookingDateTime
     * @param string $responseDateTime
     * @param array $officeHours
     * @return CarbonInterval
     */
    public function calculate($bookingDateTime, $responseDateTime, $officeHours)
    {
        $responseInterval = 0;
        $bookingDateTime = new Carbon($bookingDateTime);
        $responseDateTime = new Carbon($responseDateTime);
        $currentDateTime = clone($bookingDateTime);
        while ($currentDateTime < $responseDateTime) {
            $currentOfficeHours = $officeHours[$currentDateTime->dayOfWeek];
            if (!$currentOfficeHours['isClosed']) {
                $fromTime = Carbon::parse($currentOfficeHours['from'])
                    ->year($currentDateTime->year)
                    ->month($currentDateTime->month)
                    ->day($currentDateTime->day);
                $toTime = Carbon::parse($currentOfficeHours['to'])
                    ->year($currentDateTime->year)
                    ->month($currentDateTime->month)
                    ->day($currentDateTime->day);
                if ($currentDateTime->isSameDay($bookingDateTime) && $fromTime < $bookingDateTime) {
                    $fromTime = $bookingDateTime;
                }
                if ($currentDateTime->isSameDay($responseDateTime) && $responseDateTime < $toTime) {
                    $toTime = $responseDateTime;
                }
                $responseInterval += $toTime->diffInMinutes($fromTime);
            }
            $currentDateTime->add(new CarbonInterval(0, 0, 0, 1))->startOfDay();
        }
        return new CarbonInterval(0, 0, 0, 0, 0, $responseInterval);
    }

}
