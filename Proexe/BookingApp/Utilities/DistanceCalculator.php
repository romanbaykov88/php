<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator
{

    /**
     * @param array $from
     * @param array $to
     * @param string $unit - m, km
     *
     * @return mixed
     */
    public function calculate($from, $to, $unit = 'm')
    {
        //Took formula from here
        //https://nathanrooy.github.io/posts/2016-09-07/haversine-with-python/

        $earthRadius = 6371000; // earth radius in m.

        $phi1 = deg2rad($from[0]);
        $phi2 = deg2rad($to[0]);

        $deltaPhi = deg2rad($to[0] - $from[0]);
        $deltaLambda = deg2rad($to[1] - $from[1]);

        $a = pow(sin($deltaPhi / 2), 2) + cos($phi1) * cos($phi2) * pow(sin($deltaLambda / 2), 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        $distance = $c * $earthRadius;

        if ($unit == 'km') {
            $distance /= 1000;
        }

        return $distance;
    }

    /**
     * @param array $from
     * @param array $offices
     *
     * @return array
     */
    public function findClosestOffice($from, $offices)
    {
        $closestOffice = null;
        $closestOfficeDistance = null;
        foreach ($offices as $office) {
            $currentOfficeDistance = $this->calculate(
                [$from[0], $from[1]],
                [$office['lat'], $office['lng']]
            );
            if (!$closestOffice or $closestOfficeDistance > $currentOfficeDistance) {
                $closestOffice = $office;
                $closestOfficeDistance = $currentOfficeDistance;
            }
        }
        return $closestOffice;
    }

}

/**
 *
 * SQL solution for finding closest office would be something like this:
 *
 * SELECT `name` FROM offices ORDER BY imlement_calculate_func_here(fromLat, fromLng, `lat`, `lng`) ASC LIMIT 1
 *
 */
