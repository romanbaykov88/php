<?php

namespace Tests\Unit;

use Proexe\BookingApp\Utilities\ResponseTimeCalculator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingResponseTimeTest extends TestCase
{
    /** @test */
    public function testTask4()
    {
        $officeHours = '[{"to": "16:00", "from": "11:00", "isClosed": false}, {"to": "18:00", "from": "9:00", "isClosed": false}, {"to": "15:00", "from": "12:00", "isClosed": false}, {"to": "18:00", "from": "9:00", "isClosed": false}, {"isClosed": true}, {"to": "20:00", "from": "8:00", "isClosed": false}, {"to": "18:00", "from": "10:00", "isClosed": false}]';
        $officeHours = json_decode($officeHours, true);
        $responseTimeCalculator = new ResponseTimeCalculator();

        $this->assertEquals(1580,
            $responseTimeCalculator->calculate('2020-01-10 12:00', '2020-01-13 14:20', $officeHours)->minutes);
        $this->assertEquals(130,
            $responseTimeCalculator->calculate('2020-01-12 1:00', '2020-01-12 8:50', $officeHours)->minutes);
        $this->assertEquals(540,
            $responseTimeCalculator->calculate('2020-01-01 7:40', '2020-01-02 13:40', $officeHours)->minutes);
        $this->assertEquals(510,
            $responseTimeCalculator->calculate('2020-01-04 6:00', '2020-01-05 10:30', $officeHours)->minutes);
        $this->assertEquals(900,
            $responseTimeCalculator->calculate('2020-01-07 23:00', '2020-01-08 16:00', $officeHours)->minutes);
        $this->assertEquals(330,
            $responseTimeCalculator->calculate('2020-01-02 14:00', '2020-01-03 13:30', $officeHours)->minutes);
        $this->assertEquals(200,
            $responseTimeCalculator->calculate('2020-01-08 6:00', '2020-01-08 12:20', $officeHours)->minutes);
    }
}
